package hello;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.List;

@Controller
public class FileUploadController {
    
    @RequestMapping(value="/upload", method=RequestMethod.GET)
    public @ResponseBody String provideUploadInfo() {
        return "You can upload a file by posting to this same URL.";
    }
    
    @RequestMapping(value="/upload", method=RequestMethod.POST)
    public @ResponseBody
    JqueryUploadFileMeta handleFileUpload(@RequestParam("file") MultipartFile[] fileUploads){
        JqueryUploadFileMeta jqueryUploadFileMeta = new JqueryUploadFileMeta();
        List<FileMetadata> list = new ArrayList<FileMetadata>();

        for (MultipartFile file : fileUploads){
            FileMetadata meta = new FileMetadata();
            meta.setName(file.getOriginalFilename());
            meta.setSize(file.getSize());

            try {
                File uploadedFile = processFile(file);
            } catch (Exception e) {
                meta.setError(e.getMessage());
                e.printStackTrace();
            }finally {
                list.add(meta);
            }
        }

        jqueryUploadFileMeta.setFiles(list);
        return jqueryUploadFileMeta;
    }

    private File processFile(MultipartFile file) throws Exception {
        if (file!=null && !file.isEmpty()) {
            try {
                byte[] bytes = file.getBytes();
                File output = new File(file.getOriginalFilename());
                BufferedOutputStream stream =
                        new BufferedOutputStream(new FileOutputStream(output));
                stream.write(bytes);
                stream.close();
                System.out.println("uploaded " + output.getAbsoluteFile());
                return  output;
            } catch (Exception e) {
                throw e;
            }
        }

        return null;
    }

}
