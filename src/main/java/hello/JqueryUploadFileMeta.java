package hello;

import java.util.List;

/**
 * Created by keerati on 3/29/14 AD.
 */

public class JqueryUploadFileMeta {
    private List<FileMetadata> files;

    public List<FileMetadata> getFiles() {
        return files;
    }

    public void setFiles(List<FileMetadata> files) {
        this.files = files;
    }
}
